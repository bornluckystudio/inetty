//
//  JsonSerializerNew2.swift
//  RestClientTest2
//
//  Created by Enrico Secondulfo on 29/05/15.
//  Copyright (c) 2015 bornluckystudio. All rights reserved.
//

import UIKit

public class JsonSerializer: NSObject {
    
    public static func serialize<T:NSObject>(entity:T) -> NSData! {
        var json:NSMutableDictionary = getNormalizedObject(entity)
        
        var error:NSError?
        
        var data:NSData! = NSJSONSerialization.dataWithJSONObject(json, options: NSJSONWritingOptions.allZeros, error: &error)
        
        println("error: \(error)")
        
        return data
    }
    
    public static func deserialize<T:NSObject>(data:NSData, type: T.Type) -> [T] {
        var entities:[T] = []
        
        var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
        
        var json:AnyObject! = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: error)
        
        if (json is NSDictionary) {
            entities.append(deserialize(json as! NSMutableDictionary, type: type))
        } else if (json is NSArray) {
            entities = deserialize(json as! NSArray, type: type)
        }
        
        return entities
    }
    
    public static func deserialize<T:NSObject>(item:NSMutableDictionary, type:T.Type) -> T! {
        return deserialize(item, clazz: type) as! T
    }
    
    public static func deserialize<T:NSObject>(items:NSArray, type:T.Type) -> [T]! {
        return deserialize(items, clazz: type) as! [T]
    }
    
    private static func deserialize(item:NSMutableDictionary, clazz:AnyClass!) -> AnyObject! {
        var entity:NSObject = (clazz as? NSObject.Type)!()
        
        for (propertyName:String, propertyType:Any.Type) in getPropertyNamesAndTypes(entity) {
            
            switch propertyType {
                
            case _ as String.Type, _ as Optional<String>.Type:
                
                if (item[propertyName] is String) {
                    entity.setValue(item[propertyName], forKey: propertyName)
                }
                
            case _ as NSNumber.Type, _ as Optional<NSNumber>.Type:
                
                if (item[propertyName] is NSNumber) {
                    entity.setValue(item[propertyName], forKey: propertyName)
                }
                
                
            case _ as Optional<[String]>.Type:
                entity.setValue(deserialize((item[propertyName] as? NSArray)!, clazz: NSString.self), forKey: propertyName)
                
            case _ as Optional<[NSObject]>.Type:
                println("is array")
                
            default:
                if let entityType:AnyClass = getType(toString(propertyType)) {
                    entity.setValue(deserialize((item[propertyName] as? NSArray)!, clazz: entityType), forKey: propertyName)
                    
                }
                
                if let entityType:AnyClass = getObjectType(toString(propertyType)) {
                    entity.setValue(deserialize((item[propertyName] as? NSMutableDictionary)!, clazz: entityType), forKey: propertyName)
                }
            }
        }
        
        return entity
    }
    
    private static func deserialize(items:NSArray, clazz:AnyClass!) -> [AnyObject]! {
        var entities:[AnyObject] = []
        
        for item in items {
            var entity:AnyObject
            
            if let item = item as? NSString {
                entity = item
            } else {
                entity = deserialize(item as! NSMutableDictionary, clazz: clazz)
            }
            
            
            entities.append(entity)
        }
        
        return entities
    }
    
    public static func serializeToString<T:NSObject>(entity:T) -> String! {
        var data:NSData = serialize(entity)
        
        return NSString(data: data, encoding: NSUTF8StringEncoding) as? String
    }
    
    private static func getNormalizedObject<T:NSObject>(entity:T) -> NSMutableDictionary {
        var dictionary:NSMutableDictionary = NSMutableDictionary()
        
        for propertyName in getPropertyNames(entity.dynamicType) {
            
            if let value:AnyObject = entity.valueForKey(propertyName) {
                
                if (value is NSArray) {
                    
                    dictionary[propertyName] = getNormalizedArray(value as! [NSObject])
                    
                } else if (isNotPrimitive(value)) {
                    
                    dictionary[propertyName] = getNormalizedObject(value as! NSObject)
                    
                } else {
                    
                    dictionary[propertyName] = value
                }
            }
        }
        
        return dictionary
    }
    
    public static func getPropertyNamesAndTypes<T:NSObject>(entity:T) -> [String:Any.Type] {
        var propertyNamesAndTypes:[String:Any.Type] = [:]
        
        var entityMirror:MirrorType = reflect(entity)
        
        for var i:Int = 0; i < entityMirror.count; i++ {
            let (propertyName:String, childMirror:MirrorType) = entityMirror[i]
            
            if (propertyName == "super") {
                continue
            }
            
            propertyNamesAndTypes[propertyName] = childMirror.valueType
        }
        
        return propertyNamesAndTypes
    }
    
    
    public static func getPropertyNames<T:NSObject>(clazz:T.Type) -> [String]! {
        var propertyNames:[String] = []
        var propertiesCount:CUnsignedInt = 0
        
        var properties = class_copyPropertyList(clazz, &propertiesCount)
        
        for var i = 0; i < Int(propertiesCount); i++ {
            let property:objc_property_t = properties[i]
            
            let propertyAttributes:UnsafePointer<Int8> = property_getAttributes(property)
            let type:String! = NSString(CString: propertyAttributes, encoding: NSUTF8StringEncoding) as? String
            
            let name:String! = NSString(CString: property_getName(property), encoding: NSUTF8StringEncoding) as? String
            
            propertyNames.append(name)
        }
        
        free(properties)
        
        return propertyNames
    }
    
    private static func getNormalizedArray<T:NSObject>(entities:[T]) -> [AnyObject]! {
        var normalizedEntities:[AnyObject] = []
        
        for entity:T in entities {
            var dictionary:NSMutableDictionary = getNormalizedObject(entity)
            
            normalizedEntities.append(dictionary)
        }
        
        return normalizedEntities
    }
    
    private static func isNotPrimitive(value:AnyObject) -> Bool {
        var result:Bool = true
        
        if (value is String || value is NSNumber) {
            result = false
        }
        
        return result
    }
    
    private static func getType(typeString:String) -> AnyClass! {
        var pattern:String = "Swift.Optional<Swift.Array<(.*)>>"
        
        var regex:NSRegularExpression! = NSRegularExpression(pattern: pattern, options: NSRegularExpressionOptions.CaseInsensitive, error: nil)
        
        var textCheckingResult:NSTextCheckingResult! = regex.firstMatchInString(typeString, options: NSMatchingOptions.allZeros, range: NSMakeRange(0, count(typeString)))
        
        if textCheckingResult != nil {
            var range:NSRange = textCheckingResult.rangeAtIndex(1)
            
            var match:NSString = (typeString as NSString).substringWithRange(range)
            
            println("\(match)")
            
            return NSClassFromString(match as String)
        }
        
        return nil
    }
    
    private static func getObjectType(typeString:String) -> AnyClass! {
        let bundleID:String? = NSBundle.mainBundle().bundleIdentifier
        
        let pattern:String = "\(bundleID).(.*)>"
        
        var regex:NSRegularExpression! = NSRegularExpression(pattern: pattern, options: NSRegularExpressionOptions.CaseInsensitive, error: nil)
        
        var textCheckingResult:NSTextCheckingResult! = regex.firstMatchInString(typeString, options: NSMatchingOptions.allZeros, range: NSMakeRange(0, count(typeString)))
        
        if (textCheckingResult != nil) {
            var range:NSRange = textCheckingResult.rangeAtIndex(1)
            
            var match:NSString = (typeString as NSString).substringWithRange(range)
            
            return NSClassFromString(("\(bundleID).\(match)") as String)
        }
        
        return nil
    }
    
    
}
