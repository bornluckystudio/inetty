//
//  UploadHelper.swift
//  RestClientTest2
//
//  Created by Enrico Secondulfo on 16/05/15.
//  Copyright (c) 2015 bornluckystudio. All rights reserved.
//

import UIKit
import MobileCoreServices

public class UploadHelper: NSObject {
    
    public static func getDataForRequestUpload(filePath:String) -> NSMutableData {
        let boundary:String = getBoundary()
        
        return getDataForRequestUpload(filePath, boundary: boundary)
    }
    
    public static func getDataForRequestUpload(path:String, boundary:String) -> NSMutableData {
        let fileName:String = path.lastPathComponent
        let mimeType:String = getMimeTypeForPath(path)
        let fileData:NSData = NSData(contentsOfFile: path)!
        
        var requestData:NSMutableData = NSMutableData()
        /* requestData.appendString("------\(boundary)\r\n")
        requestData.appendString("Content-Disposition: form-data; name=\"test\";\r\n")
        requestData.appendString("Content-Type: application/json; charset=UTF-8\r\n\r\n")
        requestData.appendString("\(JsonSerializer.sharedInstance.serializeToString(test))\r\n") */
        
        
        requestData.appendString("------\(boundary)\r\n")
        requestData.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(fileName)\"\r\n")
        requestData.appendString("Content-Type: \(mimeType)\r\n\r\n")
        requestData.appendData(fileData)
        requestData.appendString("\r\n")
        requestData.appendString("------\(boundary)--")
        
        return requestData
    }
    
    public static func getDataForRequestUpload(fileData:NSData, fileName:String, boundary:String) -> NSMutableData {
        
        let mimeType:String = getMimeTypeForPath()
        
        var requestData:NSMutableData = NSMutableData()
        /* requestData.appendString("------\(boundary)\r\n")
        requestData.appendString("Content-Disposition: form-data; name=\"test\";\r\n")
        requestData.appendString("Content-Type: application/json; charset=UTF-8\r\n\r\n")
        requestData.appendString("\(JsonSerializer.sharedInstance.serializeToString(test))\r\n") */
        
        requestData.appendString("------\(boundary)\r\n")
        requestData.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(fileName)\"\r\n")
        requestData.appendString("Content-Type: \(mimeType)\r\n\r\n")
        requestData.appendData(fileData)
        requestData.appendString("\r\n")
        requestData.appendString("------\(boundary)--")
        
        return requestData
    }
    
    public static func getBoundary() -> String {
        return "Boundary\(NSUUID().UUIDString)"
    }
    
    public static func getMimeTypeForPath(path:String) -> String {
        let pathExtension:String = path.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
    
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
    
        return "application/octet-stream";
    }
    
    public static func getMimeTypeForPath() -> String {
        let pathExtension:String = "jpg"
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        
        return "application/octet-stream";
    }
   
}

extension NSMutableData {
    
    func appendString(string:String) {
        let data = string.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        appendData(data!)
    }
}
