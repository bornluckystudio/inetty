//
//  CoreDataHelper.swift
//  CoreDataTest
//
//  Created by Enrico Secondulfo on 02/05/15.
//  Copyright (c) 2015 bornluckystudio. All rights reserved.
//

import UIKit
import CoreData

public class CoreDataHelper<E:BaseManagedObject>: NSObject {
    
    private var managedObjectContext:NSManagedObjectContext?
    private var entityDescription:NSEntityDescription?
    
    convenience public init(managedObjectContext:NSManagedObjectContext!) {
        self.init()
        
        self.managedObjectContext = managedObjectContext
        self.entityDescription = NSEntityDescription.entityForName(toString(E.self).componentsSeparatedByString(".")[1], inManagedObjectContext: self.managedObjectContext!)
    }
    
    
    
    override private init() {
    }
    
    public func create() -> E {
        return E(entity: self.entityDescription!, insertIntoManagedObjectContext: self.managedObjectContext)
    }
    
    public func add(entity: E) {
        self.managedObjectContext?.save(nil)
    }
    
    public func get() -> [E] {
        return get("", value: "")
    }
    
    public func getbyKey(value:String) -> E! {
        return getFirst("key", value: value)
    }
    
    public func get(key:String, value:String) -> [E]! {
        var entities: [E]
        
        var fetchRequest:NSFetchRequest = NSFetchRequest(entityName: toString(E.self).componentsSeparatedByString(".")[1])
        
        if (key != "") {
            fetchRequest.predicate = NSPredicate(format: "%K = %@", key, value)
            
            println(fetchRequest.predicate)
        }
        
        entities = self.managedObjectContext?.executeFetchRequest(fetchRequest, error: nil) as! [E]
        
        return entities
    }
    
    public func getFirst(key: String, value: String) -> E! {
        var entities: [E] = get(key, value: value)
        
        if entities.count > 0 {
            return entities[0]
        } else {
            return nil
        }
    }
    
    public func getAll() -> [E]! {
        var entities: [E]
        
        var fetchRequest:NSFetchRequest = NSFetchRequest(entityName: toString(E.self).componentsSeparatedByString(".")[1])
        
        entities = self.managedObjectContext?.executeFetchRequest(fetchRequest, error: nil) as! [E]
        
        return entities
    }
    
    public func update(entity:E) {
        var cachedEntity:E! = getbyKey(entity.key)
        
        if cachedEntity != nil {
            cachedEntity = entity
            
            add(cachedEntity)
        }
    }
    
    public func delete(key:String) {
        let entity:E! = getbyKey(key)
        
        if (entity != nil) {
            self.managedObjectContext?.deleteObject(entity)
            self.managedObjectContext?.save(nil)
        }
    }
}
