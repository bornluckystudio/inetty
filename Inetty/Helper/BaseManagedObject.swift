//
//  BaseManagedObject.swift
//  RestClientTest2
//
//  Created by Enrico Secondulfo on 09/05/15.
//  Copyright (c) 2015 bornluckystudio. All rights reserved.
//

import UIKit
import CoreData

public class BaseManagedObject: NSManagedObject {
    
    @NSManaged public var key:String
    
    public override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }
    
    
}
