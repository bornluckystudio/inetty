//
//  HttpMethod.swift
//  RestClientTest
//
//  Created by Enrico Secondulfo on 17/04/15.
//  Copyright (c) 2015 bornluckystudio. All rights reserved.
//

import UIKit

public enum HttpMethod:String {
    
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
    case DELETE = "DELETE"
}
