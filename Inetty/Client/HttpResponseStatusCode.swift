//
//  HttpResponseStatusCode.swift
//  RestClientTest
//
//  Created by Enrico Secondulfo on 18/04/15.
//  Copyright (c) 2015 bornluckystudio. All rights reserved.
//

import UIKit

public enum HttpResponseStatusCode:Int {
    
    case OK = 200
    case CREATED = 201
    case NOT_MODIFIED = 304
    case BAD_REQUEST = 400
    case NOT_FOUND = 404
    case INTERNAL_SERVER_ERROR = 500
}
