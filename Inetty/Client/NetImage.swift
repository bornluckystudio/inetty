//
//  NetImage.swift
//  RestClientTest2
//
//  Created by Enrico Secondulfo on 09/05/15.
//  Copyright (c) 2015 bornluckystudio. All rights reserved.
//

import UIKit
import MobileCoreServices

public class NetImage: NSObject, NSURLSessionDelegate, NSURLSessionTaskDelegate {
    
    
//    public func upload(url:String, path:String) {
//        // var image:UIImage = UIImage(named: path)!
//        
//        var urlSessionConfiguration:NSURLSessionConfiguration = NSURLSessionConfiguration.ephemeralSessionConfiguration()
//        urlSessionConfiguration.HTTPMaximumConnectionsPerHost = 1
//        
//        var urlSession:NSURLSession = NSURLSession(configuration: urlSessionConfiguration, delegate: self, delegateQueue: nil)
//
//        var request:NSMutableURLRequest = createRequest(url, path: path)
//        
//        var uploadTask = urlSession.dataTaskWithRequest(request)
//        
//        uploadTask.resume()
//        
//        println(request)
//    }
    
    public func upload(url:String, fileData:NSData, fileName:String) {
        // var image:UIImage = UIImage(named: path)!
        
        var urlSessionConfiguration:NSURLSessionConfiguration = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        urlSessionConfiguration.HTTPMaximumConnectionsPerHost = 1
        
        var urlSession:NSURLSession = NSURLSession(configuration: urlSessionConfiguration, delegate: self, delegateQueue: nil)
        
        var request:NSMutableURLRequest = createRequest(url, fileData:fileData, fileName:fileName)
        
        var uploadTask = urlSession.dataTaskWithRequest(request)
        
        uploadTask.resume()
        
//        println(request)
    }
    
    public func download(url:String, successCallBack:(UIImage) -> ()) {
        var urlSessionConfiguration:NSURLSessionConfiguration = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        urlSessionConfiguration.HTTPMaximumConnectionsPerHost = 1
        
        var urlSession:NSURLSession = NSURLSession(configuration: urlSessionConfiguration, delegate: self, delegateQueue: nil)
        var taskURL:NSURL? = NSURL(string: url)
        
        var downloadTask = urlSession.downloadTaskWithURL(taskURL!, completionHandler: { (location:NSURL!, response:NSURLResponse!, error:NSError!) -> Void in
            successCallBack(UIImage(data: NSData(contentsOfURL: location)!)!)
        })
        downloadTask.resume()
        
        
        
    }
    
//    private func createRequest(url:String, path:String) -> NSMutableURLRequest {
//        let boundary:String = UploadHelper.getBoundary()
//        
//        
//        var request:NSMutableURLRequest = NSMutableURLRequest()
//        request.URL = NSURL(string: url)
//        request.HTTPMethod = "POST"
//        request.setValue("multipart/form-data; boundary=----\(boundary)", forHTTPHeaderField: "Content-Type")
//        request.HTTPBody = UploadHelper.getDataForRequestUpload(path, boundary: boundary)
//        
//        return request
//    }
    
    private func createRequest(url:String, fileData:NSData, fileName:String) -> NSMutableURLRequest {
        let boundary:String = UploadHelper.getBoundary()
        
        
        var request:NSMutableURLRequest = NSMutableURLRequest()
        request.URL = NSURL(string: url)
        request.HTTPMethod = "POST"
        request.setValue("multipart/form-data; boundary=----\(boundary)", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = UploadHelper.getDataForRequestUpload(fileData, fileName: fileName, boundary: boundary)
        
        return request
    }

    public func URLSession(session: NSURLSession, task: NSURLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
//        println("total sended bytes: \(totalBytesSent)")
//        println("total expected bytes to send: \(totalBytesExpectedToSend)")
//        println("didSendBodyData: \(bytesSent)")
    }
}


