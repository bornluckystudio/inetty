//
//  Response.swift
//  RestClientTest2
//
//  Created by Enrico Secondulfo on 02/05/15.
//  Copyright (c) 2015 bornluckystudio. All rights reserved.
//

import UIKit
import CoreData

@objc(Response)
class Response: BaseManagedObject {
   
    @NSManaged var etag:String!
    @NSManaged var data:NSData!
    @NSManaged var date:NSDate!
    
}
