//

//  RestClient2.swift

//  RestClientTest

//

//  Created by Raffaele D'Alterio on 08/09/15.

//  Copyright (c) 2015 bornluckystudio. All rights reserved.

//



import UIKit

import CoreData



public class RestClient: NSObject {
    
    
    
    let CONTENT_TYPE:String = "application/json;charset=UTF-8"
    
    let ACCEPT:String = "application/json"
    
    
    
    var baseUrl:String!
    
    var coreDataHelper:CoreDataHelper<Response>!
    
    
    
    public init(baseUrl:String, managedObjectContext:NSManagedObjectContext) {
        
        self.baseUrl = baseUrl
        
        self.coreDataHelper = CoreDataHelper<Response>(managedObjectContext: managedObjectContext)
        
    }
    
    
    
    public func doGet<T:NSObject>(entityType:T.Type, successStatusCodes:[HttpResponseStatusCode], successCallback: (([T]!, NSHTTPURLResponse!) -> ())!,errorCallback:((NSHTTPURLResponse!) -> ())!) {
        
        
        
        execute(HttpMethod.GET, url: "", successStatusCodes: successStatusCodes, successCallback: successCallback, errorCallback: errorCallback)
        
    }
    
    
    
    public func doGet<T:NSObject>(entityType:T.Type, successStatusCodes:[HttpResponseStatusCode], successCallback: ((T!, NSHTTPURLResponse!) -> ())!,errorCallback:((NSHTTPURLResponse!) -> ())!) {
        
        
        
        doGet(entityType, successStatusCodes: successStatusCodes, successCallback: { (entities:[T]!, response:NSHTTPURLResponse!) -> () in
            
            successCallback(entities?[0], response)
            
            }, errorCallback: errorCallback)
        
    }
    
    
    
    public func doGet<T:NSObject>(entityType:T.Type, successCallback: (([T]!, NSHTTPURLResponse!) -> ())!,errorCallback:((NSHTTPURLResponse!) -> ())!) {
        
        
        
        var successStatusCodes:[HttpResponseStatusCode] = [HttpResponseStatusCode.OK, HttpResponseStatusCode.NOT_MODIFIED]
        
        
        
        doGet(entityType, successStatusCodes: successStatusCodes, successCallback: successCallback, errorCallback: errorCallback)
        
    }
    
    
    
    public func doGet<T:NSObject>(entityType:T.Type, successCallback: ((T!, NSHTTPURLResponse!) -> ())!,errorCallback:((NSHTTPURLResponse!) -> ())!) {
        
        
        
        doGet(entityType, successCallback: { (entities:[T]!, response:NSHTTPURLResponse!) -> () in
            
            successCallback(entities?[0], response)
            
            }, errorCallback: errorCallback)
        
    }
    
    
    
    public func doPost<I:NSObject, O:NSObject>(entityToAdd:I!, successCallback: ((O!, NSHTTPURLResponse!) -> ())!, errorCallback: ((response:NSHTTPURLResponse!) -> ())!) {
        
        
        
        doPost(entityToAdd, successCallback: { (entities:[O]!, response:NSHTTPURLResponse!) -> () in
            
            successCallback(entities?[0], response)
            
            }, errorCallback: errorCallback)
        
    }
    
    
    
    public func doPost<I:NSObject, O:NSObject>(entityToAdd:I!, successCallback: (([O]!, NSHTTPURLResponse!) -> ())!, errorCallback: ((response:NSHTTPURLResponse!) -> ())!) {
        
        
        
        var successStatusCodes:[HttpResponseStatusCode] = [HttpResponseStatusCode.OK, HttpResponseStatusCode.NOT_MODIFIED]
        
        
        
        execute(HttpMethod.POST, url: "", formData: nil, entityToAdd: entityToAdd, successStatusCodes: successStatusCodes, successCallback: successCallback, errorCallback: errorCallback)
        
    }
    
    
    
    //    public func doPost<T:NSObject>(entityToAdd:T, successCallback: ((NSHTTPURLResponse!) -> ())!, errorCallback: ((response:NSHTTPURLResponse) -> ())!) {
    //
    //
    //
    //        //        doPost(entityToAdd, successCallback: { (entity:T!, response:NSHTTPURLResponse!) -> () in
    //
    //        //            successCallback(response)
    //
    //        //
    //
    //        //            }, errorCallback: errorCallback)
    //
    //    }
    
    
    public func doPost<I:NSObject, O:NSObject>(formData:[String:String], entityToAdd:I!, successCallback: (([O]!, NSHTTPURLResponse!) -> ())!, errorCallback: ((response:NSHTTPURLResponse!) -> ())!) {
        
        var successStatusCodes:[HttpResponseStatusCode] = [HttpResponseStatusCode.OK, HttpResponseStatusCode.NOT_MODIFIED]
        
        
        
        execute(HttpMethod.POST, url: "", formData: formData, entityToAdd: entityToAdd, successStatusCodes: successStatusCodes, successCallback: successCallback, errorCallback: errorCallback)
    }
    
    //    public func doPostOther<I:NSObject, O:NSObject>(successCallback: (([O]!, NSHTTPURLResponse!) -> ())!, errorCallback: ((response:NSHTTPURLResponse!) -> ())!) {
    //        var successStatusCodes:[HttpResponseStatusCode] = [HttpResponseStatusCode.OK, HttpResponseStatusCode.NOT_MODIFIED]
    //
    //
    //
    //
    //
    ////        let boundary:String = UploadHelper.getBoundary();
    ////        // let boundary:String = "WebKitFormBoundaryAyWQ6DgXFsXBYavR"
    ////
    ////        var requestData:NSMutableData = NSMutableData()
    ////
    ////        for (key, value) in formData {
    ////
    ////            requestData.appendString("------\(boundary)\r\n")
    ////            requestData.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
    ////            requestData.appendString("\(value)\r\n\r\n")
    ////        }
    ////
    ////        requestData.appendString("\r\n")
    ////        requestData.appendString("------\(boundary)--")
    ////
    ////        var request:NSMutableURLRequest = NSMutableURLRequest()
    ////        request.URL = NSURL(string: self.baseUrl)
    ////        request.HTTPMethod = "POST"
    ////        request.setValue("multipart/form-data; boundary=----\(boundary)", forHTTPHeaderField: "Content-Type")
    ////        request.HTTPBody = requestData
    ////
    ////        var urlSession:NSURLSession = NSURLSession(configuration: NSURLSessionConfiguration.ephemeralSessionConfiguration())
    ////
    ////
    ////
    ////        urlSession.dataTaskWithRequest(request, completionHandler: { (data:NSData!, response:NSURLResponse!, error:NSError!) -> Void in
    ////
    ////
    ////
    ////            let httpUrlResponse:NSHTTPURLResponse = response as! NSHTTPURLResponse
    ////
    ////
    ////
    ////            var success:Bool = false
    ////
    ////        }).resume()
    //
    //    }
    
    
    
    public func doPut<T:NSObject>(entityToAdd:T, successCallback: ((NSHTTPURLResponse!) -> ())!, errorCallback: ((response:NSHTTPURLResponse) -> ())!) {
        
    }
    
    
    
    public func doDelete(url:String, successCallback: ((NSHTTPURLResponse!) -> ())!, errorCallback: ((response:NSHTTPURLResponse) -> ())!) {
        
    }
    
    
    
    private func execute<T:NSObject>(httpMethod:HttpMethod, url:String, successStatusCodes:[HttpResponseStatusCode], successCallback: (([T]!, NSHTTPURLResponse!) -> ())!, errorCallback: ((response:NSHTTPURLResponse!) -> ())! ) {
        
        
        
        var urlSession:NSURLSession = NSURLSession(configuration: NSURLSessionConfiguration.ephemeralSessionConfiguration())
        
        
        
        var request:NSMutableURLRequest = createRequest(httpMethod, formData: nil, cachable: true)
        
        
        
        urlSession.dataTaskWithRequest(request, completionHandler: { (data:NSData!, response:NSURLResponse!, error:NSError!) -> Void in
            
            
            
            let httpUrlResponse:NSHTTPURLResponse = response as! NSHTTPURLResponse
            
            
            
            var success:Bool = false
            
            
            
            for httpResponseStatusCode in successStatusCodes {
                
                if (httpUrlResponse.statusCode == httpResponseStatusCode.rawValue) {
                    
                    success = true
                    
                }
                
            }
            
            
            
            if (success) {
                
                
                
                var entities:[T] = []
                
                var currentData:NSData!
                
                
                
                let url:String = toString(response.URL)
                
                let etag:String! = httpUrlResponse.allHeaderFields["Etag"] as? String
                
                
                
                if httpUrlResponse.statusCode == HttpResponseStatusCode.NOT_MODIFIED.rawValue {
                    
                    
                    
                    currentData = self.getData(url)
                    
                } else {
                    
                    
                    
                    self.saveResponse(url, etag: etag, data: data)
                    
                    currentData = data
                    
                }
                
                
                
                
                
                if currentData != nil {
                    
                    entities = JsonSerializer.deserialize(currentData, type: T.self)
                    
                }
                
                
                
                successCallback(entities, httpUrlResponse)
                
                
                
            } else {
                
                errorCallback(response: httpUrlResponse)
                
            }
            
        }).resume()
        
        
        
    }
    
    
    
    private func execute<I:NSObject, O:NSObject>(httpMethod:HttpMethod, url:String, formData:[String:String]!, entityToAdd:I!, successStatusCodes:[HttpResponseStatusCode], successCallback: (([O]!, NSHTTPURLResponse!) -> ())!, errorCallback: ((response:NSHTTPURLResponse!) -> ())!) {
        
        var urlSession:NSURLSession = NSURLSession(configuration: NSURLSessionConfiguration.ephemeralSessionConfiguration())
        
        var request:NSMutableURLRequest = createRequest(httpMethod, formData: formData, cachable: false)
        
        if (entityToAdd != nil) {
            
            request.HTTPBody = JsonSerializer.serialize(entityToAdd)
            
        }
        
        
        
        urlSession.dataTaskWithRequest(request, completionHandler: { (data:NSData!, response:NSURLResponse!, error:NSError!) -> Void in
            
            
            
            let httpUrlResponse:NSHTTPURLResponse = response as! NSHTTPURLResponse
            
            
            
            var success:Bool = false
            
            
            
            for httpResponseStatusCode in successStatusCodes {
                
                if (httpUrlResponse.statusCode == httpResponseStatusCode.rawValue) {
                    
                    success = true
                    
                }
                
            }
            
            
            
            if (success) {
                
                
                
                var entities:[O] = []
                
                
                
                if data != nil {
                    
                    entities = JsonSerializer.deserialize(data, type: O.self)
                    
                }
                
                
                
                successCallback(entities, httpUrlResponse)
                
                
                
            } else {
                
                errorCallback(response: httpUrlResponse)
                
            }
            
        }).resume()
        
    }
    
    
    
    private func createRequest(httpMethod:HttpMethod, formData:[String:String]!, cachable:Bool, url:String = "") -> NSMutableURLRequest {
        
        
        
        var request:NSMutableURLRequest = NSMutableURLRequest()
        
        request.URL = NSURL(string: "\(self.baseUrl)/\(url)")
        
        request.HTTPMethod = httpMethod.rawValue;
        
        if formData != nil {
            let boundary:String = UploadHelper.getBoundary();
            
            var requestData:NSMutableData = NSMutableData()
            
            for (key, value) in formData {
                
                requestData.appendString("------\(boundary)\r\n")
                requestData.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                requestData.appendString("\(value)\r\n\r\n")
            }
            
            requestData.appendString("\r\n")
            requestData.appendString("------\(boundary)--")
            
            request.setValue("multipart/form-data; boundary=----\(boundary)", forHTTPHeaderField: "Content-Type")
            request.HTTPBody = requestData
        }
        
        if cachable {
            
            var response:Response! = coreDataHelper.getbyKey(toString(request.URL))
            
            
            
            if response != nil {
                
                request.setValue(response.etag, forHTTPHeaderField: "If-None-Match")
                
            }
            
        }
        
        
        if (formData == nil) {
            request.setValue(CONTENT_TYPE, forHTTPHeaderField: "Content-Type")
        }
        
        request.setValue(ACCEPT, forHTTPHeaderField: "Accept")
        
        
        
        return request;
        
    }
    
    
    
    private func saveResponse(url:String, etag:String!, data:NSData!) {
        
        
        
        if let cachedResponse:Response = coreDataHelper.getbyKey(url) {
            
            
            
            cachedResponse.etag = etag
            
            cachedResponse.data = data
            
            cachedResponse.date = NSDate()
            
            
            
            self.coreDataHelper.update(cachedResponse)
            
        } else {
            
            
            
            var responseToAdd:Response = self.coreDataHelper.create()
            
            responseToAdd.key = url
            
            responseToAdd.etag = etag
            
            responseToAdd.data = data
            
            responseToAdd.date = NSDate()
            
            
            
            self.coreDataHelper.add(responseToAdd)
            
        }
        
    }
    
    
    
    private func getData(value:String) -> NSData! {
        
        var currentData:NSData!
        
        
        
        if let cachedResponse:Response = coreDataHelper.getbyKey(value) {
            
            currentData = cachedResponse.data
            
        }
        
        
        
        return currentData;
        
    }
    
}

