//
//  PartialAccount.swift
//  Inetty
//
//  Created by Enrico Secondulfo on 12/08/15.
//  Copyright (c) 2015 Born Lucky Studio s.r.l. All rights reserved.
//

import UIKit

class PartialAccount: NSObject {
   
    var username:String!
    var gender:String!
    var age:NSNumber!
    var city:String!
}
