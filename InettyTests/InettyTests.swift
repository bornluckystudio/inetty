//
//  InettyTests.swift
//  InettyTests
//
//  Created by Raffaele D'Alterio on 15/07/15.
//  Copyright (c) 2015 Born Lucky Studio s.r.l. All rights reserved.
//

import UIKit
import XCTest
import CoreData
import Inetty

class InettyTests: XCTestCase {
    
    // MARK: - Core Data stack
    
    func setUpInMemoryManagedObjectContext() -> NSManagedObjectContext {
        let managedObjectModel = NSManagedObjectModel.mergedModelFromBundles([NSBundle.mainBundle()])!
        
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        persistentStoreCoordinator.addPersistentStoreWithType(NSInMemoryStoreType, configuration: nil, URL: nil, options: nil, error: nil)
        
        let managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        
        return managedObjectContext
    }
    // MARK: - Core Data Saving support
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }
    
    func testPost() {
        
        let managedObjectContext:NSManagedObjectContext! = setUpInMemoryManagedObjectContext()
        
        let restClient:RestClient = RestClient(baseUrl: "http://192.168.0.5:8080/Instants/api/signup/social", managedObjectContext: managedObjectContext)
        
        var facebookOauth:SocialOauth = SocialOauth()
        facebookOauth.token = "CAACEdEose0cBABIV2ZAPQX4x7MSMlQYXtgVzJlBbnDZC4e0QZCXHw6MNLqiINnQplK7hP7IM0gRZAoQ7o0FT62lsIBZBdNsawZCXKIDC0OnYXXwRq9bodkdvJFE11PSx0VVOooEbeMzhXYKZAUks0lFNJRYzHCD7lEuddIgqQdLFDziPNnSbM6Pwols055bMZAVlCOWPRQLUXwZDZD"
        
        let expectation:XCTestExpectation! = self.expectationWithDescription("asynchronous request")
        
        restClient.doPost(facebookOauth, successCallback: { (partialAccount:PartialAccount!, response:NSHTTPURLResponse!) -> () in
            
            println(partialAccount)
            expectation.fulfill()
            
            }) { (response:NSHTTPURLResponse!) -> () in
                
                println("error")
                expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(30, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
}
